import 'package:rate_limiter_package/models/rate_limit_model.dart';

class RateLimiter {
  final List<RateLimitModel> _limits;
  int _lastClick = 0;
  bool _isLimited = false;

  RateLimiter(List<RateLimitModel> limits) : _limits = limits;

  Future<T?> throttle<T>(Function function) async {
    int now = DateTime.now().millisecondsSinceEpoch;
    int lastClickPeriod = _lastClick == 0 ? _lastClick : now - _lastClick;
    for (int i = 0; i < _limits.length; i++) {
      if (_limits[i].currentCount == _limits[i].maxCount) {
        if (lastClickPeriod > _limits[i].maxMs) {
          _limits[i].currentCount = 0;
        } else {
          _isLimited = true;
          return null;
        }
      } else if (!_isLimited && lastClickPeriod < _limits[i].maxMs) {
        _limits[i].currentCount += 1;
      }
    }
    _isLimited = false;
    _lastClick = now;
    return await function();
  }

  Future<T?> debounce<T>(Function func) async {
    return null;

    // TODO: implement debounce
  }
}
