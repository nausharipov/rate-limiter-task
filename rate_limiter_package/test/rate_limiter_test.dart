import 'package:flutter_test/flutter_test.dart';
import 'package:rate_limiter_package/package.dart';

void main() {
  const String _argument = 'value';
  int _count = 0;

  Future<String> _function(String value) async {
    _count += 1;
    return Future.delayed(
      const Duration(milliseconds: 5),
    ).then((_) => value);
  }

  void _resetCounter() {
    _count = 0;
  }

  group(
    'throttle',
    () {
      test('count equals to maxCount', () async {
        _resetCounter();
        const int _maxCount = 5;
        final limit = RateLimitModel(100, _maxCount);
        final _limiter = RateLimiter([limit]);
        for (var i = 0; i < _maxCount * 10; i++) {
          await _limiter.throttle<String>(() => _function(_argument));
        }
        expect(_count == limit.maxCount, true);
      });

      test('calls number is reached with an empty limiter list', () async {
        _resetCounter();
        const int callsNumber = 10;
        final _limiter = RateLimiter([]);
        for (var i = 0; i < callsNumber; i++) {
          await _limiter.throttle<String>(() => _function(_argument));
        }
        expect(_count == callsNumber, true);
      });

      test('limiter is not applied if the rate is not exceeded', () async {
        _resetCounter();
        const int taps = 10;
        final limiter = RateLimiter([
          RateLimitModel(1, 2),
          RateLimitModel(10, 3),
        ]);
        for (var i = 0; i < taps; i++) {
          await Future.delayed(const Duration(milliseconds: 15), () async {
            await limiter.throttle<String>(() => _function(_argument));
          });
        }
        expect(_count == taps, true);
      });
    },
  );
}
